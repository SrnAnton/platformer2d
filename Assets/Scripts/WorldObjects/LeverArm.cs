using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeverArm : MonoBehaviour
{
    [SerializeField] private Finish _finish;

    public void Activate()
    {
        GetComponent<SpriteRenderer>().color = new Color(150.0f, 150.0f, 150.0f);
        _finish.Activate();
    }
}
