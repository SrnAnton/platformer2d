using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Player))]
public class PlayerAttack : MonoBehaviour
{
    [SerializeField] private Animator _animator;

    private bool _isAttack;

    public bool IsAttack {get => _isAttack;}

    public void FinishAttack() => _isAttack = false;

    private void Update()
    {
        if(Input.GetMouseButtonDown(0))
        {
            _isAttack = true;
            _animator.SetTrigger("attack");
        }
    }
}
