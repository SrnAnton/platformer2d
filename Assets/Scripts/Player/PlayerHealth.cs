using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Player))]
public class PlayerHealth : MonoBehaviour
{
    [SerializeField] private float _health = 100.0f;
    [SerializeField] private Animator _animator;

    public void ReduceHealth(float damage)
    {
        _health -= damage;

        _animator.SetTrigger("takeDamage");

        if (_health <= 0.0f)
            Die();
    }

    private void Die()
    {
        _animator.SetBool("die", true);
    }
}
