using System.Diagnostics;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Debug = UnityEngine.Debug;

[RequireComponent(typeof(Rigidbody2D))]
public class Player : MonoBehaviour
{
    [SerializeField] private Animator _animator;
    [SerializeField] private float _speed = 1.0f;
    [SerializeField] private float _jumpForce = 1.0f;

    private const float _speedXMultiplier = 50.0f;
    private float _horizontal;
    private bool _isGround;
    private bool _isJump;
    private bool _isFacingRight = true;
    private bool _isFinish = false;
    private bool _isLeverArm = false;
    private bool _isSpeedBoost = false;

    private Finish _finish;
    private LeverArm _leverArm;

    private Rigidbody2D _rigidbody;

    void Start()
    {
        _rigidbody = GetComponent<Rigidbody2D>();
    }

    private void Update()
    {
        _horizontal = Input.GetAxis("Horizontal");
        _animator.SetFloat("speedX", Mathf.Abs(_horizontal));

        if(Input.GetKey(KeyCode.Space) && _isGround)
        {
            _isJump = true;
        }

        if(Input.GetKeyDown(KeyCode.F))
        {
            if(_isLeverArm)
                _leverArm?.Activate();

            if(_isFinish)
                _finish?.FinishLevel();
        }
    }

    private void FixedUpdate()
    {
        _rigidbody.velocity = new Vector2(_speed * _horizontal * _speedXMultiplier * Time.fixedDeltaTime, _rigidbody.velocity.y);
        
        if(_isJump)
        {
            _rigidbody.AddForce(new Vector2(0.0f, _jumpForce));
            _isJump = false;
            _isGround = false;
        }

        if(_horizontal > 0.0f && !_isFacingRight)
        {
            Flip(); 
        } 
        else if(_horizontal < 0.0f && _isFacingRight)
        {
            Flip();
        }

    }

    private void Flip()
    {
        _isFacingRight = !_isFacingRight;

        Vector3 playerScale = transform.localScale;
        playerScale.x *= -1;
        transform.localScale = playerScale;
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
    // Не очень ясно, что лучше, управление объектами через Teg или через создание отдельного класса
    //    if(other.gameObject.CompareTag("Ground"))
    //    {
    //        _isGround = true;
    //    }

        if(other.gameObject.TryGetComponent(out Ground ground))
        {
            _isGround = true;
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.TryGetComponent(out LeverArm leverArm))
        {
            _leverArm = leverArm;
            _isLeverArm = true;
        }

        if(other.TryGetComponent(out Finish finish))
        {
            _finish = finish;
            _isFinish = true;
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.TryGetComponent(out LeverArm leverArm))
        {
            _isLeverArm = false;
        }

        if (other.TryGetComponent(out Finish finish))
        {
            _isFinish = false;
        }
    }
}
