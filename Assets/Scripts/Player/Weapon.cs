using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour
{
    [SerializeField] private float _damage;
    private PlayerAttack _playerAttack;

    private void Start()
    {
        _playerAttack = transform.root.GetComponent<PlayerAttack>();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.TryGetComponent(out EnemyHeath enemyHeath) && _playerAttack.IsAttack)
        {
            enemyHeath.ReduceHealth(_damage);
        }
    }
}
