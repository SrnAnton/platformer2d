using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Enemy))]
public class EnemyAttack : MonoBehaviour
{
    [SerializeField] private float _enemyDamage;
    [SerializeField] private float _timeToDamage;

    private float _damageTime;
    private bool _isDamage;

    private void Update()
    {
        if(!_isDamage)
        {
            _damageTime -= Time.deltaTime;

            if(_damageTime <= 0.0f)
            {
                _damageTime = _timeToDamage;
                _isDamage = true;
            }
        }
    }

    private void OnCollisionStay2D(Collision2D other)
    {
        if(other.gameObject.TryGetComponent(out PlayerHealth playerHealth) && _isDamage)
        {
            playerHealth.ReduceHealth(_enemyDamage);
            _isDamage = false;
        }
    }
}
