using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Enemy))]
public class EnemyVision : MonoBehaviour
{
    [SerializeField] private GameObject _currentHitObject;

    [SerializeField] private float _circleRadius;
    [SerializeField] private float _maxDistance;
    [SerializeField] private LayerMask _layerMask;

    private Enemy _enemy;
    private Vector2 _origin;
    private Vector2 _direction;

    private float _currentHitDistance;

    private void Start()
    {
        _enemy = GetComponent<Enemy>();
    }

    private void Update()
    {
        _direction = _enemy.IsFacingRight? Vector2.right : Vector2.left;
        _origin = transform.position;

        RaycastHit2D hit = Physics2D.CircleCast(_origin, _circleRadius, _direction, _maxDistance, _layerMask);

        if(hit)
        {
            _currentHitObject = hit.transform.gameObject;
            _currentHitDistance = hit.distance;
            if(_currentHitObject.TryGetComponent(out Player player))
            {
                _enemy.StartChasingPlayer();
            }
        } 
        else
        {
            _currentHitObject = null;
            _currentHitDistance = _maxDistance;
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawLine(_origin, _origin + _direction * _currentHitDistance);
        Gizmos.DrawWireSphere(_origin + _direction * _currentHitDistance, _circleRadius);
    }
}
