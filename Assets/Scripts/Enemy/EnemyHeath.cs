using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Enemy))]
public class EnemyHeath : MonoBehaviour
{
    [SerializeField] private float _health = 100.0f;
    private Animator _animator;

    private void Start()
    {
        _animator = GetComponent<Animator>();
    }
    public void ReduceHealth(float damage)
    {
        _health -= damage;

        _animator.SetTrigger("takeDamage");

        if(_health <= 0.0f)
            Die();
    }

    private void Die()
    {
        gameObject.SetActive(false);
    }
}
