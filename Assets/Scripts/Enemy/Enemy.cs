using System.Transactions;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    [SerializeField] private float _walkDistance = 6.0f;
    [SerializeField] private float _minDistanceToPlayer = 2.0f;

    [Header("Patrol")]
    [SerializeField] private float _patrolSpeed = 1.0f;
    [SerializeField] private float _timeToWait = 5.0f;
    
    [Header("Chasing")]
    [SerializeField] private float _chasingSpeed = 1.0f;
    [SerializeField] private float _timeToChaise = 5.0f;
    

    private Rigidbody2D _rigidbody;
    private Vector2 _leftBoundaryPosition;
    private Vector2 _rightBoundaryPosition;
    private Vector2 _nextPoint;
    private Transform _playerTransform;
    private float _walkSpeed;
    private float _waitTime;
    private float _chaiseTime;
    private bool _isFacingRight = true;
    private bool _isWait = false;
    private bool _isChasingPLayer = false;
    // Необходима для того, чтоб Enemy не поворачивался, когда персонаж находится слишком близко
    private float _minFlipDistance = 0.2f; 

    public bool IsFacingRight
    {
        get => _isFacingRight;
    }



    private void Start()
    {
        _playerTransform = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
        _rigidbody = GetComponent<Rigidbody2D>();
        _leftBoundaryPosition = transform.position;
        _rightBoundaryPosition = _leftBoundaryPosition + Vector2.right * _walkDistance;
        _waitTime = _timeToWait;
        _chaiseTime = _timeToChaise;
        _walkSpeed = _patrolSpeed;
    }

    private void Update()
    {
        if(_isChasingPLayer)
            StartChasingTimer();

        if(_isWait && !_isChasingPLayer)
            StartWaitTimer();
      
        if(ShouldWait())
        {
            _isWait = true;
        }
    }
    private float DistanceToPlayer() => _playerTransform.position.x - transform.position.x;
    private void FixedUpdate()
    {
        _nextPoint = Vector2.right * _walkSpeed * Time.fixedDeltaTime;
        
        if(_isChasingPLayer && Mathf.Abs(DistanceToPlayer()) < _minDistanceToPlayer)
            return;

        if(_isChasingPLayer)
            ChasePlayer();

        if(ShouldPatrol())
            Patrol();
    }      

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawLine(_leftBoundaryPosition, _rightBoundaryPosition);
    }

    private void Patrol()
    {
        if (!_isFacingRight)
            _nextPoint.x *= -1.0f;

         _rigidbody.MovePosition((Vector2)transform.position + _nextPoint);
    }

    private void ChasePlayer()
    {
        float distance = DistanceToPlayer();

        if(distance < 0.0f)
            _nextPoint.x *= -1;

        if (distance > _minFlipDistance && !_isFacingRight)   
        {
            Flip();
        }
        else if (distance < _minFlipDistance && _isFacingRight)
        {
            Flip();
        }

        _rigidbody.MovePosition((Vector2)transform.position + _nextPoint);          
    }

    private void StartWaitTimer()
    {
        if (_isWait)
        {
            _waitTime -= Time.deltaTime;
            if (_waitTime < 0.0f)
            {
                _isWait = false;
                _waitTime = _timeToWait;
                Flip();
            }
        }
    }

    private void StartChasingTimer()
    {
        _chaiseTime -= Time.deltaTime;
        if (_chaiseTime < 0.0f)
        {
            _isChasingPLayer = false;
            _chaiseTime = _timeToChaise;
            _walkSpeed = _patrolSpeed;
        }
    }

    public void StartChasingPlayer()
    {
        _walkSpeed = _chasingSpeed;
        _isChasingPLayer = true;
        _chaiseTime = _timeToChaise;
    }

    private bool ShouldPatrol()
    {
        return !_isWait && !_isChasingPLayer;
    }

    private bool ShouldWait()
    {
        bool isOutOfRightBoundary = _isFacingRight && transform.position.x >= _rightBoundaryPosition.x;
        bool isOutOfLeftBoundary = !_isFacingRight && transform.position.x <= _leftBoundaryPosition.x;

        return isOutOfRightBoundary || isOutOfLeftBoundary;
    }

    private void Flip()
    {
        _isFacingRight = !_isFacingRight;

        Vector3 playerScale = transform.localScale;
        playerScale.x *= -1;
        transform.localScale = playerScale;
    }

}
